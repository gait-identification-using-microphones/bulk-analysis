# pickleWavfiles.py
This file takes the audio files and does the following:
- Reads in audio files (only set up for .wav) 
- Normalizes the frames using: `normalized = [(ele/2**8.)*2-1 for ele in a]`
- Runs the fast fourier tranform from SciPy on the normalized frames
- Stores the fft as features in a dataframe
- Takes the participant's id from the file names and puts that list in a pandas Series.
- Places the Dataframe and Series in a sklearn bunch object. (data=dataframe, target=pandasSeries)
- Pickles the sklearn Bunch object and saves it in the file name specified in `FILE_OUT`

Point these vars towards your files: 
```py
PATH_TO_RECORDINGS = './wav-clips/'
FILE_OUT = "../pickled_clips.pklz"
N = 882000
```
- `N` is the number of features you want out of your fourier transform. The padas dataframe you get back will have (N/2)-1 features 
- `PATH_TO_RECORDINGS` will be the path to your .wav files. There must not be any other file types in that directory
- `FILE_OUT` path and file name for your output pickle file

