from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.preprocessing import Normalizer

def makePCApipeline(numComponents=None):
    if numComponents == None:
        return Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('PCA',PCA() )])
    else:
        if 0 < numComponents and numComponents < 1:
            return Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('PCA',PCA(numComponents) )])
        else:
            return Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('PCA',PCA(n_components=numComponents) )])

def makeLDApipeline(numComponents=None):
    if numComponents == None:
        return Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('LDA',LDA() )])
    else:
        if 0 < numComponents and numComponents < 1:
            return Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('LDA',LDA(numComponents) )])
        else:
            return Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('LDA',LDA(n_components=numComponents) )])

def pipeline(lda=True, numComponents=None):
    if lda:
        makeLDApipeline(numComponents)
    else:
        makePCApipeline(numComponents)