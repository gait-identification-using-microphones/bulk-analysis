#!/usr/bin/env python3

from sklearn.preprocessing import StandardScaler
# broken at matplotlib 3.6
# works with matplotlib 3.3
# works with matplotlib 3.5
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
import pickle
import warnings
from sklearn.ensemble import  RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import Normalizer
from sklearn.ensemble import AdaBoostClassifier
import threading
import numpy as np

warnings.filterwarnings("ignore")  # not a good idea in general, but is appropriate for this example

# this next part will download the 'MNIST' characters from the ML (Machine Learning) datasets
# checking if I already got it:
# CHANGE THIS PATH TO WHERE YOU WANT YOUR DATA!!!!!!!!!!!
my_file_and_path = "../pickled_clips.pklz"


with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

# The images that you downloaded are contained in mnist.data and has a shape of (70000, 784) meaning there are 70,000 images with 784 dimensions (784 features).

# The labels (the integers 0–9) are contained in mnist.target. The features are 784 dimensional (28 x 28 images) and the labels are simply numbers from 0–9.
# In other words, the point is to use ML to see if it can learn from 0-9 images with 784 pixels - character recognition.
# we will do an 80% training and 20% testing
#print("Spliting")
train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2, shuffle=True)

def pipeline():
    #print("Spliting")
    train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2, shuffle=True)
    #print("normalize")
    norm = Normalizer()
    train_imgs = norm.fit_transform(train_imgs)
    test_imgs = norm.transform(test_imgs)

    # Fit on training set only.
    scaler = StandardScaler()

    #print("let's scale!")
    # Apply transform to both the training set and the test set.
    train_imgs = scaler.fit_transform(train_imgs)
    test_imgs = scaler.transform(test_imgs)

    return train_imgs, test_imgs, train_label, test_label



pca_train_imgs = 0
pca_test = 0

names = ["Random Forest", "SVC(linear)", "DecisionTree" , "LogisticRegression", "AdaBoost"]
accuracyDictionary = {}
bigDict = {}
for name in names:
    accuracyDictionary[name] = list()
    bigDict[name] = list()

def runClassifer(classifer, strName):
    #print(f"Running: {strName}")
    classifer.fit(pca_train_imgs,train_label)
    a = classifer.score(pca_test,test_label)
    a *= 100
    print(f"{strName} accuracy = {a}")
    accuracyDictionary[strName].append(a)





#print("Threading")
for rounds in range(15):
    if rounds != 0:
        print("round = ", rounds)
        for name in names:
            bigDict[name].append(accuracyDictionary[name])
            accuracyDictionary[name] = list()
    else:
        print("skipped for 1 round")

    train_imgs, test_imgs, train_label, test_label = pipeline()
    
    for p in [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]:
        classifers = [RandomForestClassifier(),  LinearSVC(), DecisionTreeClassifier(), LogisticRegression(), AdaBoostClassifier(n_estimators=100)]
        

        print(f"\nPCA = {p}")
        pca = PCA(p)
        pca_train_imgs = pca.fit_transform(train_imgs)
        pca_test = pca.transform(test_imgs)

        #run each classifer in a different thread
        threads = [ threading.Thread(target=runClassifer, args=(cl, names[i])) for i,cl in enumerate(classifers)]

        for t in threads:
            t.start()

        for t in threads:
            t.join()

print("summing")
for name in names:
    bigDict[name] = np.average(bigDict[name], axis=0)
    print(f"{name}= {bigDict[name]}")

print("Graphing")
plt.title("Accuracies of ML algroithms vs PCA %")
pcai = [ 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
for n in names:
    plt.plot(pcai, bigDict[n], label=n)
plt.legend()
plt.xlabel("PCA %")
plt.ylabel("Accuracy %")
plt.savefig("../graphs/SmallAlgorithmsVsPCAv1.png")
    
exit(0)
