#!/usr/bin/env python3

from random import Random
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
# broken at matplotlib 3.6
# works with matplotlib 3.3
# works with matplotlib 3.5
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import Normalizer
import pickle
import numpy as np
from sklearn.exceptions import ConvergenceWarning
import warnings


warnings.filterwarnings("ignore")  # not a good idea in general, but is appropriate for this example

# this next part will download the 'MNIST' characters from the ML (Machine Learning) datasets
# checking if I already got it:
# CHANGE THIS PATH TO WHERE YOU WANT YOUR DATA!!!!!!!!!!!
my_file_and_path = "../pickled_clips.pklz"


# if os.path.exists(my_file_and_path):
#     print("I already have the data, let's just load it.")
#     with gzip.open(my_file_and_path, "rb") as fh:
#         mnist = pickle.load(fh)
#     print("Loaded data into memory.")
# else:
#     print('Downloading and saving the mnist_784 ML dataset...')
#     mnist = fetch_openml('mnist_784')
#     with gzip.open(my_file_and_path, "wb") as fh:
#         pickle.dump(mnist, fh)
#     print('Downloaded and saved.')

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

# The images that you downloaded are contained in mnist.data and has a shape of (70000, 784) meaning there are 70,000 images with 784 dimensions (784 features).

# The labels (the integers 0–9) are contained in mnist.target. The features are 784 dimensional (28 x 28 images) and the labels are simply numbers from 0–9.
# In other words, the point is to use ML to see if it can learn from 0-9 images with 784 pixels - character recognition.
# we will do an 80% training and 20% testing
print("Spliting")
train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2)

print("normalize")
norm = Normalizer()
train_imgs = norm.fit_tranform(train_imgs)
test_imgs = norm.transform(test_imgs)

# Fit on training set only.
scaler = StandardScaler()

print("let's scale!")
# Apply transform to both the training set and the test set.
train_imgs = scaler.fit_transform(train_imgs)
test_imgs = scaler.transform(test_imgs)

# Make an instance of the Model that takes 95% of the variance into account (as opposed to us telling it how many components to use)
pca_percentage = 0.95
pca = PCA(pca_percentage)

print('Fitting the dataset to PCA as example at 95% variance explained.')
# pca.fit(train_imgs)

# the data has been fit, now let's transform it (i.e. do the actual work):
pca_train_imgs = pca.fit_transform(train_imgs)
pca_test_imgs = pca.transform(test_imgs)
print('Done.')

print('Running logistic regression as an example with PCA.')
# all parameters not specified are set to their defaults
# default solver is incredibly slow which is why it was changed to 'lbfgs'
logisticRegr = LogisticRegression(solver='lbfgs')

# without warnings suppressed, this does not converge. However, this is just a quick demo, so don't worry about it.
logisticRegr.fit(pca_train_imgs, train_label)

# How to predict for one observation (1 image)
# logisticRegr.predict(test_imgs[0].reshape(1, -1))

# How to predict for ten observations (10 images)
pca_pred = logisticRegr.predict(pca_test_imgs)

print(list(pca_pred))
print(test_label.to_list())
# While accuracy is not always the best metric for machine learning algorithms (precision, recall, F1 Score, ROC Curve, etc would be better), it is used here for simplicity.
print(f'The accuracy of the example logistic regression with {pca_percentage} PCA: {(accuracy_score(test_label, pca_pred) * 100):.2f}%.')


def explainedVariance(percentage, images):
    # percentage should be a decimal from 0 to 1 
    pca = PCA(percentage)
    components = pca.fit_transform(images)
    approxOriginal = pca.inverse_transform(components)
    print(f'Number of components: {pca.n_components_}')
    # print(approxOriginal.shape)
    # print(type(approxOriginal))
    return (approxOriginal, pca.n_components_)

accuracies = list()
def runLogisticRegressionTest(pca_percentage):
    if pca_percentage == 1.0:
        print(f'Accuracy without PCA (original image):')
        logisticRegr = LogisticRegression(solver='lbfgs')
        logisticRegr.fit(train_imgs, train_label)
        pca_pred = logisticRegr.predict(test_imgs)
        print(f'The accuracy of the example logistic regression with no PCA: {(accuracy_score(test_label, pca_pred) * 100):.2f}%.')
        accuracies.append((accuracy_score(test_label, pca_pred) * 100))
    else:
        print(f'Accuracy for PCA of {pca_percentage * 100}% variance explained:')
        pca = PCA(pca_percentage)
        pca_train_imgs = pca.fit_transform(train_imgs)
        pca_test_imgs = pca.transform(test_imgs)
        logisticRegr = LogisticRegression(solver='lbfgs')
        logisticRegr.fit(pca_train_imgs, train_label)
        print(pca.components_)
        pca_pred = logisticRegr.predict(pca_test_imgs)
        print(f'The accuracy of the example logistic regression with {pca_percentage} PCA: {(accuracy_score(test_label, pca_pred) * 100):.2f}%.')
        accuracies.append((accuracy_score(test_label, pca_pred) * 100))


plt.figure(figsize=(20, 4))

print('Comparing the original to PCA examples.')
ammount = [1.  , 0.95, 0.9 , 0.85, 0.8 , 0.75, 0.7 , 0.65, 0.6 , 0.55, 0.5 , 0.45, 0.4 , 0.35, 0.3 , 0.25, 0.2 , 0.15, 0.1]

for i in ammount:
    runLogisticRegressionTest(i)

plt.plot(ammount, accuracies, '-g')
plt.title("Amount of PCA vs  accuracy %")
plt.xlabel("Amount of PCA")
plt.ylabel("accuracy %")
plt.xticks(np.flip(ammount))
plt.rcParams["figure.figsize"] = (20,20)
plt.savefig("../graphs/pca.png")
