#!/usr/bin/env python3

from sklearn.preprocessing import StandardScaler
# broken at matplotlib 3.6
# works with matplotlib 3.3
# works with matplotlib 3.5
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
import pickle
import numpy as np
import warnings
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC, SVC
from sklearn.preprocessing import Normalizer
from sklearn.linear_model import SGDClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.decomposition import PCA
import threading
import pandas as pd

warnings.filterwarnings("ignore") 
my_file_and_path = "../pickled_clips.pklz"

names = ["Random Forest", "AdaBoost",  "SVC(linear)", "SGDC", "SVC(RBF)", "SVC(sigmoid)", "Logistic Regression", "MLP Classifier" ]
accuracyDictionary = {}
bigDict = {}

for name in names:
    bigDict[name] = list()
    accuracyDictionary[name] = list()

def runClassifier(classifer, strName, train_data, test_data, train_label, test_label):
    print(f"Running: {strName}")
    classifer.fit(train_data,train_label)
    a = classifer.score(test_data,test_label)
    a *= 100
    print(f"{strName} accuracy = {a}")
    accuracyDictionary[strName].append(a)


with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)




for n in range(5):
    print(f"\n\033[91mROUND: {n}\033[0m")

    for name in names:    
        accuracyDictionary[name] = list()

    for i in range(5,24):
        print(f"participants = {i}")
        participants = np.random.choice(np.unique(mnist.target), i)

        selectedTargets = list()
        newTargets = list()
        for index, t in enumerate(mnist.target):
            if t in participants:
                selectedTargets.append(index)
                newTargets.append(t)
        
        newDataframe = pd.DataFrame(mnist.data, index=selectedTargets)
        train_data, test_data, train_label, test_label = train_test_split(newDataframe, newTargets, test_size=0.2, shuffle=True)


        pipe = Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('PCA',PCA(0.2) )])
        train_data = pipe.fit_transform(train_data, train_label)
        test_data = pipe.transform(test_data)


        
        classifers = [RandomForestClassifier(), AdaBoostClassifier(),  LinearSVC(), SGDClassifier(max_iter=5000), SVC(kernel="rbf"), SVC(kernel="sigmoid"), LogisticRegression(), MLPClassifier(max_iter=1500)]
    

        threads = [ threading.Thread(target=runClassifier, args=(cl, names[i], train_data, test_data, train_label, test_label)) for i,cl in enumerate(classifers)]

        for t in threads:
            t.start()

        for t in threads:
            t.join()
    
    for name in names:
        bigDict[name].append(accuracyDictionary[name])


print("summing")
for name in names:
    bigDict[name] = np.average(bigDict[name], axis=0)
    print(name, " = ", bigDict[name])

print("\n\nGraphing")
plt.title("Accuracies of ML algorithms vs Number of Participants")
for n in names:
    plt.plot(np.arange(5,24), bigDict[n], label=n)
plt.legend()
plt.xlabel("Number of Participants")
plt.ylabel("Accuracy %")
plt.savefig("PCAalgorithVSnumParticipants.png")


exit(0)
