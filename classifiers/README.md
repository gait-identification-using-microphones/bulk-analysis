# General instructions
Point this var towards your pickled file:
```py
my_file_and_path = "../pickled_clips.pklz"
```

## PCA_with_mnist_digits.py
This takes the pickled file you point it at and does the following:
- splits the data into a training set and a test set (20%)
- Runs PCA analysis on the data using `LogisticRegression(solver='lbfgs')`
![PCA](../graphs/pca.png)

## ml_runner.py
This script tests different levels of PCA on the data compaired to different algorithms. This generated the following data graphs:
![Algorithms VS PCA](../graphs/algorithmsVSPCA.png)
![Algorithms VS PCA](../graphs/algorithmsVSPCAv2.png)
![Algorithms VS PCA](../graphs/algorithmsVSPCAv3.png)

## Number of People (numberOfPeople.py)
`numberOfPeople.py` starts with 5 participants in the dataset and slowly adds more to see how each algorithm performes. It loops 10 times to smooth out the data. This script running 10 loops took 4 days to compleate.
![Number of people VS Algorithms](../graphs/numPeople.png)
