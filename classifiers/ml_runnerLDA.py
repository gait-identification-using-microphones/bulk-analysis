#!/usr/bin/env python3

from sklearn.preprocessing import StandardScaler
# broken at matplotlib 3.6
# works with matplotlib 3.3
# works with matplotlib 3.5
import matplotlib.pyplot as plt
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.model_selection import train_test_split
import pickle
import warnings
from sklearn.ensemble import  RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import Normalizer
from sklearn.svm import LinearSVC, SVC
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
import threading
import numpy as np


warnings.filterwarnings("ignore") 
my_file_and_path = "../pickled_clips.pklz"

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)
    
names = ["Random Forest", "SVC(linear)", "SVC(RBF)", "SVC(sigmoid)", "SGDC", "DecisionTree" , "MLP", "LogisticRegression"]
bigDict = dict()
backDict = dict()
for n in names:
    bigDict[n] = list()
    backDict[n] = list()
    
for x in range(10):
    
    print("Spliting")
    train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2, shuffle=True)

    print("pipeline")
    pipe = Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('LDA',LDA(n_components=22) )])
    train_imgs = pipe.fit_transform(train_imgs, train_label)
    test_imgs = pipe.transform(test_imgs)


    classifers = [RandomForestClassifier(),  LinearSVC(), SVC(kernel="rbf"), SVC(kernel="sigmoid"), SGDClassifier(max_iter=550), DecisionTreeClassifier(), MLPClassifier(max_iter=1500), LogisticRegression()]
    

    for i, classifer in enumerate(classifers):
        classifer.fit(train_imgs, train_label)
        bigDict[names[i]].append(classifer.score(test_imgs, test_label)*100)


for n in names:
    print(f'{n} = {np.average(bigDict[n])}')


exit(0)