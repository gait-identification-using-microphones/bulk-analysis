#!/usr/bin/env python3

from sklearn.preprocessing import StandardScaler
# broken at matplotlib 3.6
# works with matplotlib 3.3
# works with matplotlib 3.5
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.model_selection import train_test_split
import pickle
import warnings
from sklearn.ensemble import  RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import Normalizer
from sklearn.svm import LinearSVC, SVC
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
import threading
import numpy as np


warnings.filterwarnings("ignore")  # not a good idea in general, but is appropriate for this example

# this next part will download the 'MNIST' characters from the ML (Machine Learning) datasets
# checking if I already got it:
# CHANGE THIS PATH TO WHERE YOU WANT YOUR DATA!!!!!!!!!!!
my_file_and_path = "../pickled_clips.pklz"


with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

# The images that you downloaded are contained in mnist.data and has a shape of (70000, 784) meaning there are 70,000 images with 784 dimensions (784 features).

# The labels (the integers 0–9) are contained in mnist.target. The features are 784 dimensional (28 x 28 images) and the labels are simply numbers from 0–9.
# In other words, the point is to use ML to see if it can learn from 0-9 images with 784 pixels - character recognition.
# we will do an 80% training and 20% testing

def pipeline():
    print("Spliting")
    train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2, shuffle=True)
    print("normalize")
    norm = Normalizer()
    train_imgs = norm.fit_transform(train_imgs)
    test_imgs = norm.transform(test_imgs)

    # Fit on training set only.
    scaler = StandardScaler()

    print("let's scale!")
    # Apply transform to both the training set and the test set.
    train_imgs = scaler.fit_transform(train_imgs)
    test_imgs = scaler.transform(test_imgs)

    return train_imgs, test_imgs, train_label, test_label


names = ["Random Forest", "SVC(linear)", "SVC(RBF)", "SVC(sigmoid)", "SGDC", "DecisionTree" , "MLP", "LogisticRegression"]
accuracyDictionary = {}
bigDict = {}

def dictSetup():
    for name in names:
        accuracyDictionary[name] = list()
        bigDict[name] = list()


test_label = list()
train_label = list()
pca_train_imgs = 0
pca_test = 0

dictSetup()



def runClassifer(classifer, strName):
    print(f"Running: {strName}")
    classifer.fit(pca_train_imgs,train_label)
    a = classifer.score(pca_test,test_label)
    a *= 100
    print(f"{strName} accuracy = {a}")
    accuracyDictionary[strName].append(a)





print("Threading")
for rounds in range(10):

    if rounds != 0:
        for name in names:
            bigDict[name].append(accuracyDictionary[name])
            accuracyDictionary[name] = list()
    else:
        print("skipped for 1 round")

    train_imgs, test_imgs, train_label, test_label = pipeline()

    for p in range(1,4):
        classifers = [RandomForestClassifier(),  LinearSVC(), SVC(kernel="rbf"), SVC(kernel="sigmoid"), SGDClassifier(max_iter=550), DecisionTreeClassifier(), MLPClassifier(max_iter=1500), LogisticRegression()]
        

        print(f"\ntSNE = {p}")
        pca = TSNE(n_components=p, n_jobs=50)
        pca_train_imgs = pca.fit_transform(train_imgs,train_label)
        pca_test = pca.fit_transform(test_imgs)

        #run each classifer in a different thread
        threads = [ threading.Thread(target=runClassifer, args=(cl, names[i])) for i,cl in enumerate(classifers)]

        for t in threads:
            t.start()

        for t in threads:
            t.join()

print("summing")
for name in names:
    bigDict[name] = np.average(bigDict[name], axis=0)

print("Graphing")
plt.figure(0)
plt.title("Accuracies of ML algroithms vs tSNE")
pcai = range(1,4)
for n in names:
    plt.plot(pcai, bigDict[n], label=n)
plt.legend()
plt.xlabel("tSNE")
plt.ylabel("Accuracy")
plt.savefig("../graphs/algorithmsVsTSNEv1HUT.png")
    

dictSetup()


print("Method 2")
for rounds in range(10):

    if rounds != 0:
        for name in names:
            bigDict[name].append(accuracyDictionary[name])
            accuracyDictionary[name] = list()
    else:
        print("skipped for 1 round")

    train_imgs, test_imgs, train_label, test_label = pipeline()

    for p in range(1,24):
        classifers = [RandomForestClassifier(),  LinearSVC(), SVC(kernel="rbf"), SVC(kernel="sigmoid"), SGDClassifier(max_iter=550), DecisionTreeClassifier(), MLPClassifier(max_iter=1500), LogisticRegression()]
        

        print(f"\ntSNE = {p}")
        pca = TSNE(n_components=p, method='exact', n_jobs=50)
        pca_train_imgs = pca.fit_transform(train_imgs,train_label)
        pca_test = pca.fit_transform(test_imgs)

        #run each classifer in a different thread
        threads = [ threading.Thread(target=runClassifer, args=(cl, names[i])) for i,cl in enumerate(classifers)]

        for t in threads:
            t.start()

        for t in threads:
            t.join()

print("summing")
for name in names:
    bigDict[name] = np.average(bigDict[name], axis=0)

print("Graphing")
plt.figure(1)
plt.title("Accuracies of ML algroithms vs tSNE")
pcai = range(1,24)
for n in names:
    plt.plot(pcai, bigDict[n], label=n)
plt.legend()
plt.xlabel("tSNE")
plt.ylabel("Accuracy")
plt.savefig("../graphs/algorithmsVsTSNEv1EXACT.png")
    
exit(0)
