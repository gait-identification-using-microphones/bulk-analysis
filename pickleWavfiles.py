#!/usr/bin/env python3

from sklearn.utils import Bunch
from sklearn.preprocessing import normalize
import pandas as pd
import numpy as np
import pickle
import os
import re
import scipy.fftpack as fft
from scipy.io import wavfile # get the api
import warnings

warnings.filterwarnings("ignore")


PATH_TO_RECORDINGS = './wav-clips/'
FILE_OUT = "../pickled_clips.pklz"
N = 882000


print("Generating empty DataFrame and Target np array")
target = np.zeros(len(os.listdir(PATH_TO_RECORDINGS)), dtype=np.int32)
df = pd.DataFrame(columns=np.arange(0,(N/2)-1))
print("Done")

print("wav -> DataFrame")
x = True
for i , fname in enumerate(os.listdir(PATH_TO_RECORDINGS)):


    print(f"{i} out of 1,375", end="\r")
    fs, data = wavfile.read(PATH_TO_RECORDINGS+fname)
    a = data.T[0]

    smoothed = [(ele/2**8.)*2-1 for ele in a]
    transformed = fft.fft(smoothed, n=N)
    transformed = transformed
    
    d = int(len(transformed)/2)
    finalList = abs(transformed[:(d-1)])

    

    df.loc[len(df.index)] = finalList
    target[i] = int(re.search('p(.+?)-b',fname).group(1))


    

print("\n",df.head())
print(len(df.index))

print("Bunching and Pickling")
dataset = Bunch(data=df, target=pd.Series(target))

with open(FILE_OUT, 'wb') as pickled:
    pickle.dump(dataset, pickled, protocol=pickle.HIGHEST_PROTOCOL)
