#!/usr/bin/env python3

from random import random
from sklearn.preprocessing import StandardScaler
# broken at matplotlib 3.6
# works with matplotlib 3.3
# works with matplotlib 3.5
import matplotlib.pyplot as plt
import pickle
import numpy as np
import warnings
import threading
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.cluster import KMeans, AffinityPropagation, MeanShift, DBSCAN, Birch, AgglomerativeClustering, SpectralClustering, OPTICS
from sklearn.metrics.cluster import completeness_score
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.utils import Bunch

warnings.filterwarnings("ignore") 
my_file_and_path = "../pickled_clips.pklz"
FILE_OUT = "clusterResults.pklz"
N_CLUSTERS = 8

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

#the algorthems i am using
print("Init...")
names = ["KMeans", "AffinityPropagation", "MeanShift", "DBSCAN", "Birch", "Agglomerative", "Spectral", "OPTICS", "GaussianMixture"]
algs = [KMeans(n_clusters=N_CLUSTERS, max_iter=500), AffinityPropagation(max_iter=500), MeanShift(n_jobs=5,max_iter=500), DBSCAN(n_jobs=5), Birch(n_clusters=N_CLUSTERS), AgglomerativeClustering(n_clusters=N_CLUSTERS), SpectralClustering(n_clusters=N_CLUSTERS), OPTICS(n_jobs=5), GaussianMixture(n_components=N_CLUSTERS)]
#this is for later
algorithmResults = {}

print("Pipeline")
pipe = Pipeline([('scaler', StandardScaler()), ('PCA', PCA(0.8))])
X = pipe.fit_transform(mnist.data, mnist.target)
# X2 = pipe.fit_transform(mnist.backpack)



def cluster(cluster, strName, i):
    print(f"Running: {strName}")
    #cluster.fit(X)
    plt.bar(i, completeness_score(mnist.target, cluster.fit_predict(X)*100,label=str(strName)))



for i, alg in enumerate(algs):
    cluster(alg, names[i], i)


plt.title(f"Cluster Completeness Score")
plt.ylabel(f"ompleteness %")
plt.legend(loc=4)
plt.tick_params(bottom = False, labelbottom=False)
plt.savefig(f"../graphs/Cluster-Completeness-Score-N{bunch.n_clusters}.png")


exit(0)
