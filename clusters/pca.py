import pickle
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from consistency import consistency
import numpy as np
import pandas as pd
import warnings

warnings.filterwarnings("ignore") 
my_file_and_path = "./backpacktests/pickled_backpack.pklz"

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

print("pipeline")
pipe = Pipeline([('scaler', StandardScaler()), ('PCA', PCA(n_components=2))])
X = pipe.fit_transform(mnist.data, mnist.target)
X2 = pipe.transform(mnist.backpack)

plt.figure(0)
plt.title("PCA cluster with seperation of backpacks")
plt.scatter(X[:,0], X[:,1], c='red', marker='o')
plt.scatter(X2[:,0], X2[:,1], c='turquoise', marker='+')
plt.legend()
plt.savefig("../graphs/pca-backpack.png")

exit(0)
print("clustering")
k = KMeans(n_clusters=5, max_iter=1000)
results = k.fit_predict(X)

store = dict()
for l in np.unique(mnist.target):
    store[l] = list()

for i, result in enumerate(results):
    store[mnist.target[i]].append(result)

plt.figure(1)
for i, key in enumerate(store):
    plt.bar(i, np.unique(store[key]))

plt.title("Unique number of (KMeans) clusters")
plt.yticks(ticks=[1,2,3,4])
plt.savefig("../graphs/pcaNumClusterCount-n1300.png")

fig = plt.figure(2)
ax = fig.add_subplot()
for i, key in enumerate(store):
    values, counts = np.unique(store[key], return_counts=True)
    hight = np.max(counts)/np.sum(counts)
    ax.bar(i, hight)
    ax.set_title("Rate of most common cluster")
    ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    plt.savefig("../graphs/pcaBarRateOfMostCommon-N1300.png")

exit(0)
