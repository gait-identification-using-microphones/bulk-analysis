import pickle
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import StandardScaler
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import numpy as np
import warnings

warnings.filterwarnings("ignore") 
my_file_and_path = "../pickled_clips.pklz"

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

# print(np.unique(mnist.target))
# exit(0)

print("Scaling and LDA...")
pipe = Pipeline([('Normalizer', Normalizer()),('scaler', StandardScaler())])
X = pipe.fit_transform(mnist.data, mnist.target)

print("linking...")
z = linkage(X, 'ward')

print("dendrogram")
plt.title("Dendrogram after LDA")
fig = plt.figure(figsize=(75, 10))
dn = dendrogram(z, labels=np.array(mnist.target))

print("saving")
plt.savefig("../graphs/Dendrogram.png")
exit(0)