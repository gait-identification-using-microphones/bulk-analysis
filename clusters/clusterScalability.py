#!/usr/bin/env python3
import pandas as pd
import pickle
import numpy as np
import warnings
from sklearn.cluster import KMeans, AffinityPropagation, MeanShift, DBSCAN, Birch, AgglomerativeClustering, SpectralClustering, OPTICS
from sklearn.mixture import GaussianMixture
from sklearn.metrics.cluster import v_measure_score
from sklearn.model_selection import train_test_split
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore") 
my_file_and_path = "../pickled_clips.pklz"
N_CLUSTERS = 10

names = ["KMeans", "AffinityPropagation", "MeanShift", "DBSCAN", "Birch", "Agglomerative", "Spectral", "OPTICS", "GaussianMixture"]


accuracyDictionary = {}
bigDict = {}
for name in names:
    bigDict[name] = list()
    accuracyDictionary[name] = list()

def runClassifier(classifer, strName, train_data, test_data, train_label, test_label):
    print(f"Running: {strName}")
    classifer.fit(train_data,train_label)
    a = v_measure_score(classifer.fit_predict(test_data), test_label)
    a *= 100
    print(f"{strName} accuracy = {a}")
    accuracyDictionary[strName].append(a)

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

for n in range(5):
    print(f"\n\033[91mROUND: {n}\033[0m")

    for name in names:    
        accuracyDictionary[name] = list()

    for i in range(5,24):
        print(f"participants = {i}")
        participants = np.random.choice(np.unique(mnist.target), i)

        selectedTargets = list()
        newTargets = list()
        for index, t in enumerate(mnist.target):
            if t in participants:
                selectedTargets.append(index)
                newTargets.append(t)
        
        newDataframe = pd.DataFrame(mnist.data, index=selectedTargets)
        train_data, test_data, train_label, test_label = train_test_split(newDataframe, newTargets, test_size=0.2, shuffle=True)


        pipe = Pipeline([('Normalizer', Normalizer()), ('StandardScaler', StandardScaler()), ('LDA',LDA() )])
        train_data = pipe.fit_transform(train_data, train_label)
        test_data = pipe.transform(test_data)
        algs = [KMeans(n_clusters=N_CLUSTERS, max_iter=500), AffinityPropagation(max_iter=500), MeanShift(n_jobs=5,max_iter=500), DBSCAN(n_jobs=5), Birch(n_clusters=N_CLUSTERS), AgglomerativeClustering(n_clusters=N_CLUSTERS), SpectralClustering(n_clusters=N_CLUSTERS), OPTICS(n_jobs=5), GaussianMixture(n_components=N_CLUSTERS)]
    

        # threads = [ threading.Thread(target=runClassifier, args=(cl, names[i], train_data, test_data, train_label, test_label)) for i,cl in enumerate(algs)]

        # for t in threads:
        #     t.start()

        # for t in threads:
        #     t.join()
        for i, cl in enumerate(algs):
            runClassifier(cl, names[i], train_data, test_data, train_label, test_label)
    
    for name in names:
        bigDict[name].append(accuracyDictionary[name])


print("summing")
for name in names:
    bigDict[name] = np.average(bigDict[name], axis=0)
    print(name, " = ", bigDict[name])

print("\n\nGraphing")
plt.title("V-Measure vs Number of Participants")
for n in names:
    plt.plot(np.arange(5,24), bigDict[n], label=n)
plt.legend()
plt.xlabel("Number of Participants")
plt.ylabel("V Measure Score")
plt.savefig("../graphs/clustersVSscalability.png")


exit(0)