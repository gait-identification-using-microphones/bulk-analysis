import pickle
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.manifold import TSNE
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd

import warnings

warnings.filterwarnings("ignore") 
my_file_and_path = "./backpacktests/pickled_backpack.pklz"

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2, shuffle=True)

print("pipeline")
pipe = Pipeline([('scaler', StandardScaler()), ('TSNE', TSNE(n_components=1, n_iter=2000))])
X = pipe.fit_transform(mnist.data, mnist.target)
X2 = pipe.fit_transform(mnist.backpack, mnist.targetBack)

plt.figure(0)
plt.title("t-SNE clusters comparing backpacks vs no backpacks")
plt.scatter(X[:,0], X[:,1], c='red', marker='o')
plt.scatter(X2[:,0], X2[:,1], c='turquoise', marker='+')
plt.legend()
plt.savefig("../graphs/tsne-backpack.png")
exit(0)

# print("t-SNE")
# x = TSNE(n_components=3, n_iter=2000).fit_transform(scale_train)


k = KMeans(n_clusters=5, max_iter=1000)
results = k.fit_predict(x)

store = dict()
for l in np.unique(mnist.target):
    store[l] = list()

for i, result in enumerate(results):
    store[mnist.target[i]].append(result)

plt.figure(0)
for i, key in enumerate(store):
    plt.bar(i, len(np.unique(store[key])))

# print("piping")
# pipe = Pipeline([StandardScaler(), PCA(n_components=50), TSNE(n_components=2, learning_rate=50, n_iter=2000)])

# x = pipe.fit_transform(mnist.data)

# plt.scatter(x[:,0], x[:,1])
plt.title("Unique number of clusters")
plt.yticks(ticks=[1,2,3,4,5,6])
plt.savefig("../graphs/tsneBarcount.png")

fig = plt.figure(1)
ax = fig.add_subplot()
for i, key in enumerate(store):
    values, counts = np.unique(store[key], return_counts=True)
    hight = np.max(counts)/np.sum(counts)
    ax.bar(i, hight)
    ax.set_title("Rate of most common cluster")
    ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    plt.savefig("../graphs/tsneBarRateOfMostCommon.png")



exit(0)