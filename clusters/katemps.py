#!/usr/bin/env python3
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import pickle
import warnings
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

warnings.filterwarnings("ignore") 
my_file_and_path = "../pickled_clips.pklz"

with open(my_file_and_path, 'rb') as fh:
    mnist = pickle.load(fh)

print("Splitting")
#spliting the data
train_imgs, test_imgs, train_label, test_label = train_test_split(mnist.data, mnist.target, test_size=0.2, shuffle=True)

print("Scaling")
scaler = StandardScaler()
scale_train = scaler.fit_transform(train_imgs)
scale_test = scaler.transform(test_imgs)

print("PCA time")
pca = PCA(n_components=2)
pca_train = pca.fit_transform(scale_train)
pca_test = pca.transform(scale_test)


plt.scatter(pca_train[:,0], pca_train[:,1], label="training")
plt.scatter(pca_test[:,0], pca_test[:,1], label='testing')
plt.legend()
plt.savefig("precluster.png")

# Call the k-means:
# for cluster in range(20, 25):
cluster = 24
kmeans = KMeans(n_clusters=cluster, random_state=0) # set 'random_state' to a seed to make the results deterministic 
kmeans.fit(scale_train)

labels = kmeans.labels_
print(f"\nK-means labels for {cluster} clusters: {kmeans.labels_}, which translates to the following clusters:")
for my_cluster in range(cluster):
    print(f"\nClassifcation {my_cluster}:\n__________")
    for i, kmeans_label in enumerate(labels):
        if my_cluster == kmeans_label:
            print(train_label[i])

sns.lmplot('z1', 'z2', data=pca_train, fit_reg=False, hue="clusters", scatter_kws={"marker": "D", "s": 100})
plt.savefig(f"cluster-{cluster}.png")

# # Show the visual results of 'cluster' clusters:


# plt.title(f'Height vs Weight - {cluster} clusters')
# plt.xlabel('Height')
# plt.ylabel('Weight')
# plt.savefig(f"cluster-{cluster}.png")

# k = KMeans(n_clusters=24)

# print("looping")
# k.fit(pca_train, y=train_label)
# kt = k.get_feature_names_out()
# print(kt)
# print("Labels: ", k.labels_)

# for index,row in test_imgs.iterrows():
#     row = [row]
#     result = k.predict(row)
#     print(result)


exit(0)



