#!/usr/bin/env python3

from sklearn.utils import Bunch
from sklearn.preprocessing import normalize
import pandas as pd
import numpy as np
import pickle
import os
import re
import scipy.fftpack as fft
from scipy.io import wavfile # get the api
import warnings

warnings.filterwarnings("ignore")


PATH_TO_RECORDINGS = '../../wav-clips/'
FILE_OUT = "./pickled_backpack.pklz"
N = 882000


print("Generating empty DataFrame and Target np array")
targetT = list()
targetF = list()
dfT = pd.DataFrame(columns=np.arange(0,(N/2)-1))
dfF = pd.DataFrame(columns=np.arange(0,(N/2)-1))
print("Done")

print("wav -> DataFrame")
x = True
for i , fname in enumerate(os.listdir(PATH_TO_RECORDINGS)):

    try:
        print(f"{i} out of {len(os.listdir(PATH_TO_RECORDINGS))}", end="\r")
        fs, data = wavfile.read(PATH_TO_RECORDINGS+fname)
        a = data.T[0]
    except:
        continue

    smoothed = [(ele/2**8.)*2-1 for ele in a]
    transformed = fft.fft(smoothed, n=N)
    transformed = transformed
    
    d = int(len(transformed)/2)
    finalList = abs(transformed[:(d-1)])

    #print(bool(re.search('-b(False|True)-',fname).group(1))
    if "True" in re.search('-b(False|True)-',fname).group(1):
        dfT.loc[len(dfT.index)] = finalList
        targetT.append(int(re.search('p(.+?)-b',fname).group(1)))
    else:
        dfF.loc[len(dfF.index)] = finalList
        targetF.append(int(re.search('p(.+?)-b',fname).group(1)))

    

print("\n",dfT.head())
print(len(dfT.index))

print("\n",dfF.head())
print(len(dfF.index))


print("Bunching and Pickling")
dataset = Bunch(data=dfF, target=pd.Series(targetF), backpack=dfT, targetBack=pd.Series(targetT))

with open(FILE_OUT, 'wb') as pickled:
    pickle.dump(dataset, pickled, protocol=pickle.HIGHEST_PROTOCOL)
