import matplotlib.pyplot as plt
import numpy as np
import pickle
import warnings


warnings.filterwarnings("ignore") 
# my_file_and_path = "../pickled_clips.pklz"
my_file_and_path = "clusterResults.pklz"


with open(my_file_and_path, 'rb') as fh:
    bunch = pickle.load(fh)

print(bunch.results['KMeans'])
print(type(bunch.results['KMeans']))



for i, alg in enumerate(bunch.results):

    store = dict()
    for t in np.unique(bunch.target):
        store[t] = list()

    for j, t in enumerate(bunch.target):
        store[t].append(bunch.results[alg][j])
    
    main = 0
    total = 0
    for p in store:
        values, counts = np.unique(store[p], return_counts=True)
        main += np.max(counts)
        total += np.sum(counts)
    
    print(alg, " ", main/total*100, "%")
    plt.bar(i, main/total*100, label=str(alg))

plt.title(f"Cluster Consitancy with PCA (k={bunch.n_clusters})")
plt.ylabel(f"Consistancy %")
plt.legend(loc=4)
plt.tick_params(bottom = False, labelbottom=False)
plt.savefig(f"../graphs/backpack-PCA-ClustersN{bunch.n_clusters}.png")

exit(0)
